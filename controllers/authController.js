const User = require('../models/user')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const accessToken = require('../config');


const regUser = async (req,res) => {
    try {
        const { name, login, password } = req.body;
    if (!(login && password && name)) {
      res.status(400).send("All input is required");
    }
    const oldUser = await User.findOne({ login });
    if (oldUser) {
      return res.status(409).send("User Already Exist. Please login");
    }
    role = req.body.role;
    encryptedPassword = await bcrypt.hash(password, 5);
    const user = await User.create({
      name,
      login: login.toLowerCase(), 
      password: encryptedPassword,
      role: role,
    });
    const token = jwt.sign(
      { user_id: user._id, login },
        accessToken,
      {
        expiresIn: "2h",
      }
    );
    user.token = token;
    res.status(201).json(user);
  } catch (err) {
    res.send(408).send("you are dsdasdsaf")
  }
};


const loginUser = async (req, res) => {
  try {
    const { login, password } = req.body;
    if (!(login && password)) {
      res.status(400).send("All input is required");
    }
    const user = await User.findOne({ login });
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign(
        { user_id: user._id, login },
          accessToken,
        {
          expiresIn: "2h",
        }
      );
      user.token = token;
      res.status(200).json(user);
    }
    res.status(400).send("Invalid Credentials");
  } catch (err) {
    res.send(408).send("you are dsdasdsaf")
  }
};

module.exports = { regUser, loginUser };