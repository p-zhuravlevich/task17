const express = require("express");
const categoryRout = express.Router();
const auth = require ("../auth.js");
const categoryController = require("../controllers/categoryController.js");




categoryRout.post("/create",auth.verifyToken,auth.isAdmin,categoryController.createCategory);
categoryRout.delete("/:id",auth.verifyToken,auth.isAdmin, categoryController.deleteCategory);
categoryRout.put("/:id", auth.verifyToken,auth.isAdmin, categoryController.updateCategory);
categoryRout.get("/:id", auth.verifyToken,categoryController.getСategoryById);
categoryRout.get("/get/all", auth.verifyToken,categoryController.getAllСategories);

module.exports = categoryRout;
